package me.wuwenbin.noteblogv5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author wuwenbin
 */
@SpringBootApplication
@EnableScheduling
@EnableCaching
public class Noteblogv5Application {

    public static void main(String[] args) {
        SpringApplication.run(Noteblogv5Application.class, args);
    }

}
