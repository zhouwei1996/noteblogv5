package me.wuwenbin.noteblogv5.service.impl.msg;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.wuwenbin.noteblogv5.mapper.MessageMapper;
import me.wuwenbin.noteblogv5.model.bo.MessageBo;
import me.wuwenbin.noteblogv5.model.entity.Message;
import me.wuwenbin.noteblogv5.service.interfaces.msg.MessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author wuwen
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {


    @Override
    public IPage<MessageBo> findMessagePage(IPage<MessageBo> page, String nickname, String clearComment, boolean enable) {
        return this.baseMapper.findMessagePage(page, nickname, clearComment, enable);
    }

    @Override
    public MessageBo findLatestMessage() {
        return this.baseMapper.findLatestMessage();
    }

    @Override
    public long findTodayMessage() {
        return this.baseMapper.findTodayMessage();
    }
}
