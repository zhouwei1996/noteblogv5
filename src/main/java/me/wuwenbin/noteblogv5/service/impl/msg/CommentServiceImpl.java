package me.wuwenbin.noteblogv5.service.impl.msg;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.wuwenbin.noteblogv5.mapper.CommentMapper;
import me.wuwenbin.noteblogv5.model.bo.CommentBo;
import me.wuwenbin.noteblogv5.model.bo.ReplyBo;
import me.wuwenbin.noteblogv5.model.entity.Comment;
import me.wuwenbin.noteblogv5.service.interfaces.msg.CommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author wuwen
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {



    @Override
    public IPage<CommentBo> findCommentPage(IPage<CommentBo> page,
                                            String nickname, String clearComment,
                                            List<String> articleIds, Long userId, boolean enable) {
        return this.baseMapper.findCommentPage(page, nickname, clearComment, articleIds, userId, enable);
    }

    @Override
    public CommentBo findLatestComment() {
        return this.baseMapper.findLatestComment();
    }

    @Override
    public long findTodayComment() {
        return this.baseMapper.findTodayComment();
    }

    @Override
    public IPage<ReplyBo> findReplyPage(IPage<ReplyBo> page, Long userId) {
        return this.baseMapper.findReplyPage(page, userId);
    }
}
