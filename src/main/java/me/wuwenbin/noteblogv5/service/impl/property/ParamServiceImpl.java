package me.wuwenbin.noteblogv5.service.impl.property;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.wuwenbin.noteblogv5.constant.NBV5;
import me.wuwenbin.noteblogv5.constant.OperateType;
import me.wuwenbin.noteblogv5.constant.RoleEnum;
import me.wuwenbin.noteblogv5.constant.UploadConstant;
import me.wuwenbin.noteblogv5.mapper.ParamMapper;
import me.wuwenbin.noteblogv5.mapper.UserCoinRecordMapper;
import me.wuwenbin.noteblogv5.mapper.UserMapper;
import me.wuwenbin.noteblogv5.model.ResultBean;
import me.wuwenbin.noteblogv5.model.entity.Param;
import me.wuwenbin.noteblogv5.model.entity.User;
import me.wuwenbin.noteblogv5.model.entity.UserCoinRecord;
import me.wuwenbin.noteblogv5.service.interfaces.property.ParamService;
import me.wuwenbin.noteblogv5.util.CacheUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * created by Wuwenbin on 2019-08-05 at 11:46
 *
 * @author wuwenbin
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ParamServiceImpl extends ServiceImpl<ParamMapper, Param> implements ParamService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserCoinRecordMapper userCoinRecordMapper;


    @Override
    public void saveInitParam(Map<String, Object> map) {
        CacheUtils.clearAllParamCache();
        for (Map.Entry<String, Object> next : map.entrySet()) {
            String key = next.getKey();
            if (!"username".equals(key) && !"password".equals(key)) {
                if (!StringUtils.isEmpty(next.getValue())) {
                    String value = (String) next.getValue();
                    update(Wrappers.<Param>lambdaUpdate().set(Param::getValue,value).eq(Param::getName,key));
                }
            }
        }
    }


    @Override
    public void initMasterAccount(String username, String password, String email) {
        String nickname = "nbv5_user";
        User user = User.builder()
                .role(RoleEnum.ADMIN)
                .nickname(nickname)
                .password(SecureUtil.md5(password))
                .username(username)
                .avatar("/static/assets/img/nbv5.png")
                .email(email)
                .build();
        int i = userMapper.insert(user);
        if (i > 0) {
            update(Wrappers.<Param>lambdaUpdate().set(Param::getValue,NBV5.ADMIN_INIT_STATUS).eq(Param::getName,"1"));
            update(Wrappers.<Param>lambdaUpdate().set(Param::getValue,nickname).eq(Param::getName,NBV5.MAIL_SENDER_NAME));
            update(Wrappers.<Param>lambdaUpdate().set(Param::getValue,nickname).eq(Param::getName,NBV5.INFO_LABEL_NICKNAME));
            update(Wrappers.<Param>lambdaUpdate().set(Param::getValue,user.getAvatar()).eq(Param::getName,NBV5.INFO_LABEL_LOGO));
            userCoinRecordMapper.insert(
                    UserCoinRecord.builder().operateTime(new Date()).operateType(OperateType.INIT_REG)
                            .operateValue(0).remainCoin(0).remark(OperateType.INIT_REG.getDesc())
                            .userId(user.getId()).build()
            );
        }
    }

    @Override
    @Cacheable(value = "paramCache", key = "'init_status'")
    public Param getInitStatus() {
        return this.baseMapper.selectOne(Wrappers.<Param>query().eq("name", NBV5.INIT_STATUS));
    }

    @Override
    @Cacheable(value = "paramCache", key = "#name")
    public Param findByName(String name) {
        return this.baseMapper.selectOne(Wrappers.<Param>query().eq("name", name));
    }

    @Override
    public ResultBean updateSettings(String name, String value) {
        if (name.equalsIgnoreCase(NBV5.IS_OPEN_OSS_UPLOAD)) {
            CacheUtils.removeParamCache(NBV5.UPLOAD_TYPE);
            final String type = "0".equalsIgnoreCase(value) ? UploadConstant.Method.LOCAL.name() : UploadConstant.Method.QINIU.name();
            this.update(new LambdaUpdateWrapper<Param>().set(Param::getValue, type).eq(Param::getName, NBV5.UPLOAD_TYPE));
        }
        if (name.equalsIgnoreCase(NBV5.COMMENT_MAIL_NOTICE_ONOFF)
                || name.equalsIgnoreCase(NBV5.MESSAGE_MAIL_NOTICE_ONOFF)
                || name.equalsIgnoreCase(NBV5.USER_SIMPLE_REG_ONOFF)) {
            String adminEmail = userMapper.selectOne(Wrappers.<User>query().eq("role", RoleEnum.ADMIN.getValue())).getEmail();
            if (StrUtil.isEmpty(adminEmail)) {
                return ResultBean.error("请先设置管理员邮箱！");
            } else {
                // 查询基本信息: host port from（邮箱地址） user pass
                List<String> infoList = Arrays.asList(new String[]{NBV5.MAIL_SMPT_SERVER_ADDR, NBV5.MAIL_SMPT_SERVER_PORT, NBV5.MAIL_SERVER_ACCOUNT, NBV5.MAIL_SENDER_NAME, NBV5.MAIL_SERVER_PASSWORD});
                Integer count = this.baseMapper.selectCount(new LambdaQueryWrapper<Param>().in(Param::getName, infoList));
                if(count != 5){return ResultBean.error("请先设置完整发送邮件服务器信息！");}

            }
        }
        return update(name, value);
    }

    @Override
    public ResultBean updateMailConfig(List<Param> params) {
        params.forEach(param -> this.update(new LambdaUpdateWrapper<Param>().set(Param::getValue,param.getValue()).eq(Param::getName,param.getName())));
        CacheUtils.clearAllParamCache();
        return ResultBean.ok("更新邮件服务器配置成功！");
    }

    @Override
    public long calcRunningDays() {
        Param param = this.baseMapper.selectOne(Wrappers.<Param>query().eq("name", "system_init_datetime"));
        String initDateStr = param.getValue();
        Date initDate = DateUtil.parse(initDateStr);
        Date now = DateUtil.parse(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return DateUtil.between(initDate, now, DateUnit.DAY);
    }
}
