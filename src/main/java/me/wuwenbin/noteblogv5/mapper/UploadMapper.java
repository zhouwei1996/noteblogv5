package me.wuwenbin.noteblogv5.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import me.wuwenbin.noteblogv5.annotation.Mapper;
import me.wuwenbin.noteblogv5.model.entity.Upload;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wuwen
 */
@Mapper
public interface UploadMapper extends BaseMapper<Upload> {
}
