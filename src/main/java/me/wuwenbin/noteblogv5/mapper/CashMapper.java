package me.wuwenbin.noteblogv5.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.wuwenbin.noteblogv5.model.entity.Cash;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wuwen
 */
@Mapper
public interface CashMapper extends BaseMapper<Cash> {
}
